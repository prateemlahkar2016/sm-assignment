({
    initializeView : function(component) {
        component.set('v.options', '');
        component.set('v.currentPage', 1);
    },
    getData : function(component) {
        
        var query = component.get('v.query');
        var currentPage = component.get('v.currentPage');
        
        var actionParams = {
            query : query,
            pageNo : currentPage,
            recsPerPage : 10
        };
        
        var actionCallBack = function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                component.set('v.loading', false);
                var options = [];
                var result = response.getReturnValue();
                if(result.totalPages == 0) {
                    component.set('v.dataSet', '');
                    component.set('v.options', '');
                }
                else {
                    component.set('v.dataSet', result.recordSet);
                    for(var count = 1; count <= result.totalPages; count++) {
                        options.push({ 'label': '' + count, 'value': count });
                    }
                    component.set('v.options', options);
                }   
            } else if (state === "ERROR") {
                this.handleError(response.getError());
            }
        };
        
        component.set('v.loading', true);
        this.callAction(component, 'c.getRecords', actionParams, actionCallBack);
    }
})