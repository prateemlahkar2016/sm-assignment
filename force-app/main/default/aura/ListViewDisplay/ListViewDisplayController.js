({
	handleListViewSelectedEvent : function(component, event, helper) {
		var sObjIcon = event.getParam("sObjectIcon");
        var sObjName = event.getParam("sObjectLabel");
        var query = event.getParam("ListViewQuery");
        var columns = event.getParam("ListViewColumns");
        
        component.set("v.sObjectName", sObjName);
        component.set("v.sObjectIcon", sObjIcon);
        component.set("v.query", query);
        component.set("v.columns", columns);
        
        helper.initializeView(component);   
        helper.getData(component);    
	},
    paginate: function(component, event, helper) {
    	helper.getData(component);
	},
    updateDataTable: function(component, event, helper) {
        helper.getData(component);
    }
})