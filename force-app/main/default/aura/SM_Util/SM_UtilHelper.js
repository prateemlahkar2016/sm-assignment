({
	fireApplicationEvent : function(eventName, eventParams) {
        var applnEvent = $A.get(eventName);
        if(eventParams !== undefined && eventParams !== null) applnEvent.setParams(eventParams);  
        applnEvent.fire();
    },
    
    callAction : function(component, actionName, actionParams, callbackFunction) {
        var action = component.get(actionName);
        
        if(actionParams !== undefined && actionParams !== null) action.setParams(actionParams);  
        action.setCallback(this, callbackFunction);
        
        $A.enqueueAction(action);      
    },
    
    fireToastEvent : function(title, message, type) {

        var toastEvent = $A.get("e.force:showToast");
        var params = {};
        params["message"] = message;
        if(title !== null && title !== "") params["title"] = title;
        if(type !== null && type !== "") params["type"] = type;
        
        toastEvent.setParams(params);
        toastEvent.fire();
    },
    
    handleError : function(message) {
        this.fireToastEvent('ERROR!', message, 'error');
        console.error(message);
    }
})