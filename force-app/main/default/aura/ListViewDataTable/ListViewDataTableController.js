({            
	handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        
        switch (action.name) {
            case 'edit_record':
                var rowSelectedEvent = $A.get('e.c:ListViewRowSelected');
                rowSelectedEvent.setParams({
                    "Record_Id" : row.Id,
                    "RecordName" : row.Name
                });
                rowSelectedEvent.fire();
                break;
            default:
                console.log(row);
                break;
        }
    }
})