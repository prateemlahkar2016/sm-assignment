({
	fireEvents : function() {
        this.fireToastEvent("Success!", "Record updated successfully!", "success");    
        this.fireApplicationEvent('e.c:SM_RecordUpdated', null);
	}
})