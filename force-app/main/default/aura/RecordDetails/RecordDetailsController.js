({
	openRecordEdit : function(component, event, helper) {
		var recId = event.getParam("Record_Id");
        var recName = event.getParam("RecordName");
        component.set('v.recordId', recId);
        component.set('v.RecordName', recName);
	},
    
    initializeView : function(component, event, helper) {
        var fields = event.getParam("sObjectFields");
        var soIcon = event.getParam("sObjectIcon");
        var soAPIName = event.getParam("sObjectAPIName");
        
        component.set("{!v.recordId}", '');
        component.set("{!v.sObjectType}", '');
        
        component.set("{!v.fields}", fields);
        component.set("{!v.sObjectType}", soAPIName);
        component.set("{!v.sObjectIcon}", soIcon);
    },
    
    handleRecordSuccess : function(component, event, helper) {
		helper.fireEvents();
    }
})