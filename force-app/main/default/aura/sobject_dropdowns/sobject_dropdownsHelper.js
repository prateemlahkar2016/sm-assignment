({
    getSObjectTypes : function(component) {
             
        var actionCallback = function(response) {
            var sObjectInfos = [];
			var state = response.getState();
            if(state === "SUCCESS") {        
                var result = response.getReturnValue();
                for(var index in result) {
                    sObjectInfos.push({ 
                        apiName : result[index].sObjectAPIName, 
                        label : result[index].sObjectLabel, 
                        icon : result[index].sObjectIcon 
                    });
                }            
                component.set('v.listOfsObjectInfo', sObjectInfos);
            }
            else if (state === "ERROR") {
                this.handleError(response.getError());
            }
        }
        
        this.callAction(component, 'c.getSObjectTypes', null, actionCallback);     
    },
    
    getListViewsForsObject : function(component) {
        var sObjAPIName = component.get('v.selectedSObjectAPIName');
        
        var actionParams = {
            sObjAPIName : sObjAPIName
        };
         
        var actionCallBack = function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                var listViews = [];
                for(var listViewId in result) {
                    var label = result[listViewId]; 
                    listViews.push({ key : listViewId, value : label });
                }
                component.set('v.listOfListViews', listViews);
            }
            else if (state === "ERROR") {
                this.handleError(response.getError());
            }
        }
       
        this.callAction(component, 'c.getListViewsForsObject', actionParams, actionCallBack);
    },
    
    getListViewDescribe : function(component, lViewId) {
        
        var sObjAPIName = component.get('v.selectedSObjectAPIName');
        var sObjInfos = component.get('v.listOfsObjectInfo');
        var sObjIcon, sObjLabel;
        
        for(var index in sObjInfos) {
            if(sObjInfos[index].apiName == sObjAPIName) {
                sObjIcon = sObjInfos[index].icon;
                sObjLabel = sObjInfos[index].label;
            }
        }
        var actionParams = {
            sObjAPIName : sObjAPIName,
            listViewId : lViewId
        };
        
        var actionCallback = function(response) {
            
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                var query = result.query;
                var resultCols = result.columns;
                
                var columns = [{ 
                    label: 'Edit',                             
                    type: 'button',                             
                    initialWidth: 80,                            
                    typeAttributes: {                                 
                        label: 'Edit',                                  
                        name: 'edit_record', 
                        title: 'Click to Edit Record'             
                    }                      
                }];
                var fields = [];
                for (var key in resultCols) {
                    columns.push({ label: resultCols[key], fieldName: key, type: 'text'});
                    fields.push(key);
                }
                
                var eventParams = { 
                    "sObjectLabel" : sObjLabel,
                    "sObjectIcon" : sObjIcon,
                    "sObjectAPIName" : sObjAPIName,
                    "sObjectFields" : fields,
                    "ListViewQuery" : query,
                    "ListViewColumns" : columns                                            
                };            
                this.fireApplicationEvent("e.c:ListViewSelected", eventParams);
            }
            else if (state === "ERROR") {
                this.handleError(response.getError());
            }
        }

        this.callAction(component, 'c.getListViewDescribe', actionParams, actionCallback);    
    }
})