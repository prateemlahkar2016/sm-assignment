({
    retrieveSObjects : function(component, event, helper) {     
        helper.getSObjectTypes(component);
    },
    
    retrieveListViews : function(component, event, helper) {
        component.set('v.selectedListViewId', '');  
        helper.getListViewsForsObject(component);
    },
    
    handleListViewSelection : function(component, event, helper) {
        var lViewId = component.get('v.selectedListViewId');    
             
        if(lViewId !== null && lViewId !== undefined && lViewId !== "") {     
            helper.getListViewDescribe(component, lViewId);
        }
    }
})