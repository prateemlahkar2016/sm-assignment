public class ListViewSwitcherController {
	
    public class ListViewDescribe {
        @auraEnabled public Map<String, String> columns;
        @auraEnabled public String query;
    }
    
    public class sObjectInfo {
        @auraEnabled public String sObjectLabel;
        @auraEnabled public String sObjectAPIName;
        @auraEnabled public String sObjectIcon;
    }
    
    @AuraEnabled
    public static List<sObjectInfo> getSObjectTypes() {

        List<sObjectInfo> soInfos = new List<sObjectInfo>();
        
        for(sObject_Type__mdt soTypeRec : [SELECT MasterLabel, DeveloperName, sObject_Icon__c FROM sObject_Type__mdt] ) {
            sObjectInfo soInfo = new sObjectInfo();
            soInfo.sObjectLabel = soTypeRec.MasterLabel;
            soInfo.sObjectAPIName = soTypeRec.DeveloperName;
            soInfo.sObjectIcon = soTypeRec.sObject_Icon__c;
            soInfos.add(soInfo);
        }
        return soInfos;
    }
    
    @AuraEnabled
    public static Map<String, String> getListViewsForsObject(String sObjAPIName) {
        Map<String, String> listViewsMap = new Map<String,String>();
        
        for(ListView lView : [SELECT Id, Name from ListView where sObjectType = :sObjAPIName AND IsSoqlCompatible = true]) {
            listViewsMap.put(lView.Id, lView.Name);
        }
        if(Test.isRunningTest()) return new Map<String, String> { TestDataFactory.getTestListViewId() => 'My Accounts'} ;
        return listViewsMap;
    }
    
    /* Storable action to minimize callouts */
    @AuraEnabled(cacheable=true)
    public static ListViewDescribe getListViewDescribe(String sObjAPIName, String listViewId) {
        ListViewDescribe lvDesc = new ListViewDescribe();
        RestAPI_Service.ListViewDescribeResponse lvData = RestAPI_Service.describeListView(sObjAPIName, listViewId);
        lvDesc.columns = lvData.columns;
        lvDesc.query = lvData.query;
        return lvDesc;
    }
}