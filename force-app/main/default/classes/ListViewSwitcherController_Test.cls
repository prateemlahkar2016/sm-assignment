@isTest
public class ListViewSwitcherController_Test {
    
    
    /* Testing a typical use case of the switcher */
    @isTest
    static void testListViewSwitcher() {
        
        
        Test.setMock(HttpCalloutMock.class, TestDataFactory.getListViewDesccribeMock());
        Test.startTest();

        
        System.assert(!ListViewSwitcherController.getSObjectTypes().isEmpty());
        
        // When component is loaded, sObjects are populated
        String sObjectAPIName;
        for(ListViewSwitcherController.sObjectInfo soInfo : ListViewSwitcherController.getSObjectTypes()) {
            if(soInfo.sObjectLabel == 'Account') sObjectAPIName = soInfo.sObjectAPIName;
        }
        
        // User chooses sObject, list views are displayed
        Map<String, String> listViewsForAccount = ListViewSwitcherController.getListViewsForsObject(sObjectAPIName);
        System.assertEquals(listViewsForAccount.get(TestDataFactory.getTestListViewId()), TestDataFactory.getTestListViewName());
        
        // User chooses list view, and its details are fetched
        ListViewSwitcherController.ListViewDescribe lvDescribe = ListViewSwitcherController.getListViewDescribe(sObjectAPIName, TestDataFactory.getTestListViewId());
        System.assert(!lvDescribe.columns.isEmpty());
        System.assert(!String.isBlank(lvDescribe.query));
        
        Test.stopTest();
        
    }
}