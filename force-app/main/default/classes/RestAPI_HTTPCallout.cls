public class RestAPI_HTTPCallout {

    private static final String HOST_NAME = Url.getSalesforceBaseUrl().toExternalForm() + '/services/data/v45.0';  
    
    public static String callout(String resourceURL) {
        Http h = new Http();
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(HOST_NAME + resourceURL);
        req.setHeader('Authorization', 'Bearer ' + getSessionIDFromVF());
        req.setMethod('GET');
        
        HttpResponse res = h.send(req);  
        return res.getBody();
    }
    
    private static String getSessionIDFromVF(){

         PageReference sessionPage = Page.SessionIDPage;
         String pageContent = Test.isRunningTest() ? 'SessionID_Start<SessionId>SessionID_End' : sessionPage.getContent().toString();

         Integer sessionIdStart = pageContent.indexOf('SessionID_Start') + 'SessionID_End'.length();
         Integer sessionIdEnd = pageContent.indexOf('SessionID_End');

         String sessionId = pageContent.substring(sessionIdStart, sessionIdEnd);
         return sessionId;
    }
}