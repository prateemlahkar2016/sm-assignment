public with sharing class ListViewDisplayController {
    
    public class RecordDataSet {
        @auraEnabled public List<sObject> recordSet;
        @auraEnabled public Integer totalPages;
    }
    
    @AuraEnabled
    public static RecordDataSet getRecords(String query, Integer pageNo, Integer recsPerPage) {
        
        // Since we are displaying only 100 records for a list view
        query += ' LIMIT 100';
        List<sObject> queryResult = Database.query(query);
        
        RecordDataSet rds = new RecordDataSet();
        if(queryResult == null || queryResult.isEmpty()) {			
            rds.recordSet = new List<sObject>();
            rds.totalPages = 0;
            return rds;
        }
        
        rds.recordSet = queryResult.size() < recsPerPage? queryResult : getDataSet(queryResult, pageNo, recsPerPage);
        rds.totalPages = (Integer) Math.ceil((Double) queryResult.size() / recsPerPage);
        return rds;
    }
    
    private static List<sObject> getDataSet(List<sObject> queryResult, Integer pageNo, Integer recsPerPage) {
        Integer offset = (pageNo - 1) * recsPerPage;
        List<sObject> dataSet = new List<sObject>();
        
        for(Integer index = offset; index < (offset + recsPerPage); index++) {
            if(index >= queryResult.size()) break;
            dataSet.add(queryResult.get(index));
        }   
        return dataSet;
    }
}