@isTest
public class RestAPI_Service_Test {
    
    @isTest
    static void test_describeListView() {
        
        String sObjectType = 'Account';
        StaticResourceCalloutMock srMock = TestDataFactory.getListViewDesccribeMock();
        String listViewId = TestDataFactory.getTestListViewId();
        
        Test.setMock(HttpCalloutMock.class, srMock);
        
        Test.startTest();
        
        RestAPI_Service.ListViewDescribeResponse lvDesc = RestAPI_Service.describeListView(sObjectType, listViewId);
        
        Test.stopTest();
        
        System.assert(!lvDesc.columns.isEmpty());
        System.assert(!String.isBlank(lvDesc.query));
    }
    
    
}