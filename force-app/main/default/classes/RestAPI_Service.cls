public class RestAPI_Service {
    
    public class ListViewDescribeResponse {        
        public Map<String, String> columns; 
        public String query;        
    }
    
    public static ListViewDescribeResponse describeListView(String sObjectType, String listViewId) {
        System.debug('Request:' + sObjectType + ' ' + listViewId);
        
        String resourceURI = '/sobjects/' + sObjectType + '/listviews/' + listViewId + '/describe';
        
        ListViewDescribeResponse respObj = new ListViewDescribeResponse();
        respObj.columns = new Map<String, String>();
        
        String response = RestAPI_HTTPCallout.callout(resourceURI);
        
        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response);
        respObj.query = (String) responseMap.get('query');
        
        List<Object> columns = (List<Object>) responseMap.get('columns');
        for(Object columnObj : columns) {
            Map<String, Object> columnData = (Map<String, Object>) columnObj;
            
            if(!(Boolean) columnData.get('hidden')) {              
                respObj.columns.put((String) columnData.get('fieldNameOrPath'), (String) columnData.get('label'));
            }
        }
        return respObj;
    }
    
}