public class TestDataFactory {

    public static StaticResourceCalloutMock getListViewDesccribeMock() {
        StaticResourceCalloutMock srMock = new StaticResourceCalloutMock();
        srMock.setStaticResource('ListViewEditor_MockResponse');
        srMock.setHeader('Content-Type', 'application/json');
        srMock.setStatusCode(200);
        
        return srMock;
    }
    
    public static String getTestListViewId() {
        return '00BD0000005WcBe';
    }
    
    public static String getTestListViewName() {
        return 'My Accounts';
    }
    
    public static String getTestListViewQuery() {
        return 'SELECT name, site, billingstate, phone, tolabel(type), owner.alias, id, createddate, lastmodifieddate, systemmodstamp' 
                + ' FROM Account'
                + ' WHERE CreatedDate = THIS_WEEK ORDER BY Name ASC NULLS FIRST, Id ASC NULLS FIRST';
    }
    
    public static void createAccounts(Integer noOfRecords) {
        
        List<Account> accList = new List<Account>();
        
        for(Integer index = 0; index < noOfRecords; index++) {
            Account acc = new Account(Name = 'Test Account ' + index);
            accList.add(acc);
        }
        
        insert accList;
    }
}