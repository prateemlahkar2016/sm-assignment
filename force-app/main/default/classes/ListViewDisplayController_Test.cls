@isTest
public class ListViewDisplayController_Test {
    
    @testSetup
    static void setupTestData() {
        TestDataFactory.createAccounts(100);
    }
    
    @isTest
    static void testListViewControllerWithNoRecords() {
        
        String query = 'SELECT ID, Name, StageName FROM Opportunity';
        Integer pageNo = 1, recsPerPage = 10;
        
        Test.startTest();
        ListViewDisplayController.RecordDataSet rds = ListViewDisplayController.getRecords(query, pageNo, recsPerPage);
        
        System.assert(rds.recordSet.isEmpty());
        System.assertEquals(rds.totalPages, 0);
        Test.stopTest();
    }
    
    @isTest
    static void testListViewController() {
        String query = TestDataFactory.getTestListViewQuery();
        Integer pageNo = 1, recsPerPage = 10;
        
        Test.startTest();
        ListViewDisplayController.RecordDataSet rds = ListViewDisplayController.getRecords(query, pageNo, recsPerPage);
        Test.stopTest();
        
        System.assertEquals(rds.recordSet.size(), 10);
        System.assertEquals(rds.totalPages, 10);
    }
}